# Hot Wire Cutter


## Hot wire for cutting foam - Hardware Description


![pic](https://gitlab.com/tolocar/hot-wire-cutter/-/raw/main/image_2024-03-06_16-44-33__1_.jpg){width=1200px} 

## Technology
### Комплектуючі.

Блок живлення регульований (3-12 В, 2А) - 1шт.

Штекер 2,1*5,5мм. - 1шт.

Фанера 8мм. (500*800) - 0,5 м.кв.

Ніхромовий дріт 0,2мм. - 1+м.п.

Дріт мідний багатожильний 2м. (1м - червоний, 1м.- чорний)

Клеми з кільцем під зажим - 2шт.

Шайба М5* 15*мм - 4шт.

Гвинт М5*30мм - 3шт.

Гвинт "Мебельний" М5*20мм. -2шт.

Гайка М5 - 3шт.

Гайка “баранчик” М5 - 2шт.

Термоусадка 2,5-3 мм. - 4см.



### Інструмент та обладнання:
Паяльник, олово, каніфоль. 

Лазерний різак. 3Д принтер. (або замовити послугу)

Ноутбук/комп'ютер для завантаження файлу на порізку.

Кусачки для дроту. 

Обтискачі для клем.

## Key Ressources


## License

Hardware design, CAD and PCB files, BOM, settings and other technical or design files are released under the following license:

CERN Open Hardware Licence Version 2 Weakly Reciprocal - CERN-OHL-W


Assembly manual, pictures, videos, presentations, description text and other type of media are released under the following license:

Creative-Commons-Attribution-ShareAlike 4.0 International - CC BY-SA 4.0

всі файли для побудови струни - качайте з папочки....
